<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Edit group</title>
    </head>

    <body>
        <h1>Edit group</h1>

        <form action="{{ action('GroupController@update', $group) }}" method="post">
            {{ csrf_field() }}
            {{ method_field('PUT') }}
            <label>Customer group id:</label>
            <input type="number" value="{{$group->customer_group_id}}" name="customer_group_id" placeholder="Customer group id">
            <br><label>Customer group code:</label>
            <input type="text" value="{{$group->customer_group_code}}" name="customer_group_code" placeholder="Customer group code">
            <br><label>Tax class id:</label>
            <input type="number" value="{{$group->tax_class_id}}" name="tax_class_id" placeholder="Tax class id">
            <input type="submit" name="submit" value="Edit">
        </form>
    </body>
</html>