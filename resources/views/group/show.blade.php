<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Show Groups</title>
    </head>

    <body>
        <h1>Shoe groups</h1>
        <table>
            <tr>
                <td>Customer group id</td>
                <td>Customer group code</td>
                <td>Tax class is</td>
                <td>Edit</td>
                <td>Delete</td>
            </tr>
            
            @foreach($groups as $group)
            <tr>
                <td>{{$group->customer_group_id}}</td>
                <td>{{$group->customer_group_code}}</td>
                <td>{{$group->tax_class_id}}</td>
            </tr>
            @endforeach
        </table>
    </body>
</html>