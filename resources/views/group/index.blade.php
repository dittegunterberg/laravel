<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Groups</title>
    </head>

    <body>
        <h1>Groups</h1>
        <table>
            <tr>
                <td>Customer group id</td>
                <td>Customer group code</td>
                <td>Tax class is</td>
                <td>Edit</td>
                <td>Delete</td>
            </tr>
            
            @foreach($groups as $group)
            <tr>
                <td>{{$group->customer_group_id}}</td>
                <td>{{$group->customer_group_code}}</td>
                <td>{{$group->tax_class_id}}</td>
                <td><form action="{{action('GroupController@edit', $group->customer_group_id)}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field("GET") }}
                        <input type="submit" value="Edit"/>
                </form></td>
                <td><form action="{{action('GroupController@destroy', $group->customer_group_id)}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field("DELETE") }}
                        <input type="submit" value="Delete"/>
                </form></td>
            </tr>
            @endforeach
        </table>
    </body>
</html>