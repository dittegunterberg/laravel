<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Create group</title>
    </head>

    <body>
        <h1>Create product</h1>

        <form action="{{ action('GroupController@store') }}" method="post">
            {{ csrf_field() }}
            <input type="number" name="customer_group_id" placeholder="Customer group id">
            <input type="text" name="customer_group_code" placeholder="Customer group code">
            <input type="number" name="tax_class_id" placeholder="Tax class id">
            <input type="submit" name="submit" value="send">
        </form>
    </body>
</html>