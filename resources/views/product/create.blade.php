<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Create product</title>
    </head>

    <body>
        <h1>Create product</h1>

        <form action="{{ action('ProductController@store') }}" method="post">
            {{ csrf_field() }}
            <input type="number" name="entity_id" placeholder="entity id">
            <input type="text" name="sku" placeholder="sku">
            <input type="text" name="name" placeholder="name">
            <input type="text" name="price" placeholder="price">
            <input type="number" name="is_in_stock" placeholder="is in stock">
            <input type="submit" name="submit" value="send">
        </form>
    </body>
</html>