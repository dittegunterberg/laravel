<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Products</title>
    </head>

    <body>
        <h1>Products</h1>
        <table>
            <tr>
                <td>Entity_id</td>
                <td>Sku</td>
                <td>Name</td>
                <td>Price</td>
                <td>Is in stock</td>
                <td>Edit</td>
                <td>Delete</td>
            </tr>
            
            @foreach($products as $product)
            <tr>
                <td>{{$product->entity_id}}</td>
                <td>{{$product->sku}}</td>
                <td>{{$product->name}}</td>
                <td>{{$product->price}}</td>
                <td>{{$product->is_in_stock}}</td>
                <td><form action="{{action('ProductController@edit', $product->entity_id)}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field("GET") }}
                        <input type="submit" value="Edit"/>
                </form></td>
                <td><form action="{{action('ProductController@destroy', $product->entity_id)}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field("DELETE") }}
                        <input type="submit" value="Delete"/>
                </form></td>
            </tr>
            @endforeach
        </table>
    </body>
</html>