<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Edit products</title>
    </head>

    <body>
        <h1>Edit product</h1>

        <form action="{{ action('ProductController@update', $product) }}" method="post">
            {{ csrf_field() }}
            {{ method_field('PUT') }}
            <label>Entity id:</label>
            <input type="number" value="{{$product->entity_id}}" name="entity_id" placeholder="entity id">
            <br><label>SKU:</label>
            <input type="text" value="{{$product->sku}}" name="sku" placeholder="sku">
            <br><label>Name:</label>
            <input type="text" value="{{$product->name}}" name="name" placeholder="name">
            <br><label>Price:</label>
            <input type="text" value="{{$product->price}}" name="price" placeholder="price">
            <br><label>Is in stock:</label>
            <input type="number" value="{{$product->is_in_stock}}" name="is_in_stock" placeholder="is in stock">
            <input type="submit" name="submit" value="Edit">
        </form>
    </body>
</html>