<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->unsignedBigInteger('id');
            $table->smallInteger('order_id')->nullable();
            $table->bigInteger('item_id')->nullable();
            $table->datetime('created_at')->nullable();
            $table->datetime('updated_at')->nullable();
            $table->string('name')->nullable();
            $table->string('sku')->nullable();
            $table->bigInteger('qty')->nullable();
            $table->decimal('price')->nullable();
            $table->decimal('tax_amount')->nullable();
            $table->decimal('row_total')->nullable();
            $table->decimal('price_incl_tax')->nullable();
            $table->decimal('total_incl_tax')->nullable();
            $table->decimal('tax_percent')->nullable();
            $table->smallInteger('amount_package')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}