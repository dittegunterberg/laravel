<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->string('increment_id');
            $table->datetime('created_at')->nullable();
            $table->datetime('updated_at')->nullable();
            $table->smallInteger('customer_id')->nullable();
            $table->string('customer_email')->nullable();
            $table->string('status')->nullable();
            $table->string('marking')->nullable();
            $table->decimal('grand_total')->nullable();
            $table->decimal('subtotal')->nullable();
            $table->decimal('tax_amount')->nullable();
            $table->smallInteger('billing_address_id')->nullable();
            $table->smallInteger('shipping_address_id')->nullable();
            $table->string('shipping_method')->nullable();
            $table->decimal('shipping_amount')->nullable();
            $table->decimal('shipping_tax_amount')->nullable();
            $table->string('shipping_description')->nullable();
            $table->smallInteger('id');
            $table->string('invoice_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}