<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->unsignedBigInteger('id');
            $table->string('email')->nullable();
            $table->string('firstname')->nullable();
            $table->string('lastname')->nullable();
            $table->smallInteger('gender')->nullable();
            $table->smallInteger('customer_activated')->nullable();
            $table->smallInteger('group_id')->nullable();
            $table->string('customer_company')->nullable();
            $table->smallInteger('company_id')->nullable();
            $table->smallInteger('default_billing')->nullable();
            $table->smallInteger('default_shipping')->nullable();
            $table->smallInteger('is_active')->nullable();
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->string('customer_invoice_email')->nullable();
            $table->string('customer_extra_text')->nullable();
            $table->smallInteger('customer_due_date_period')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}