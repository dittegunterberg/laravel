<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigInteger('entity_id', false, true)->primary();
            $table->smallInteger('entity_type_id')->nullable();
            $table->smallInteger('attribute_set_id')->nullable();
            $table->string('type_id')->nullable();
            $table->string('sku')->nullable();
            $table->smallInteger('has_options')->nullable();
            $table->smallInteger('required_options')->nullable();
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->smallInteger('status')->nullable();
            $table->string('name')->nullable();
            $table->smallInteger('amount_package')->nullable();
            $table->decimal('price')->nullable();
            $table->smallInteger('is_salable')->nullable();
            $table->smallInteger('is_in_stock')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
