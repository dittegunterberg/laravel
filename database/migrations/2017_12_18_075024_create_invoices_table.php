<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->unsignedBigInteger('id', true);
            $table->datetime('invoice_date')->nullable();
            $table->datetime('expiry_date')->nullable();
            $table->smallInteger('customer_id')->nullable();
            $table->decimal('total_excl_vat')->nullable();
            $table->decimal('vat')->nullable();
            $table->decimal('shipping_excl_vat')->nullable();
            $table->decimal('shipping_vat')->nullable();
            $table->decimal('grand_total')->nullable();
            $table->boolean('invoiced')->nullable();
            $table->string('serial_number')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
