<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/customers', 'CustomersController@index');

Route::get('/customers/{id}', 'CustomersController@getById');

Route::get('/customers/{customer_id}/address', 'CustomersController@customerAddress');

Route::get('/customers/by-company/{company_id}', 'CustomersController@byCompany');

Route::get('/fb-login', 'FacebookController@index');

Route::get('/login', 'FacebookController@loginForm');

Route::get('/facebook', 'FacebookController@facebook');

Route::resource('products', 'ProductController');

Route::resource('groups', 'GroupController');

Route::resource('group_prices', 'GroupPriceController');

Route::get('/stripe', 'StripeController@index');