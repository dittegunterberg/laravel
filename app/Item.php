<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    // Länka modellen till en annan tabell
    protected $table = 'items';
    
    // Primary key-kolumnen antas vara id
    protected $primaryKey = 'id';
    
    // Primary key-kolumnen antas vara auto-inkrementerande
    public $incrementing = false;

    // Vi vitlistar kolumner
    protected $fillable = [
        "id",
        "order_id",
        "item_id",
        "created_at",
        "updated_at",
        "name",
        "sku",
        "qty",
        "price",
        "tax_amount",
        "row_total",
        "price_incl_tax",
        "total_incl_tax",
        "tax_percent",
        "amount_package"
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
}