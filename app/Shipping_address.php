<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shipping_address extends Model
{
    // Länka modellen till en annan tabell
    protected $table = 'shipping_addresses';
    
    // Primary key-kolumnen antas vara id
    protected $primaryKey = 'id';
    
    // Primary key-kolumnen antas vara auto-inkrementerande
    public $incrementing = false;

    public $timestamps = false;

    // Vi vitlistar kolumner
    protected $fillable = [
        "id",
        "customer_id",
        "customer_address_id",
        "email",
        "firstname",
        "lastname",
        "postcode",
        "street",
        "city",
        "telephone",
        "country_id",
        "address_type",
        "company",
        "country"
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
}
