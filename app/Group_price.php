<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group_price extends Model
{
    // Länka modellen till en annan tabell
    protected $table = 'group_prices';
    
    // Primary key-kolumnen antas vara id
    protected $primaryKey = 'price_id';
    
    // Primary key-kolumnen antas vara auto-inkrementerande
    public $incrementing = false;

    public $timestamps = false;

    // Vi vitlistar kolumner
    protected $fillable = [
        "price",
        "group_id",
        "price_id",
        "product_id"
    ];

    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}