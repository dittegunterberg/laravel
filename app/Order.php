<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    // Länka modellen till en annan tabell
    protected $table = 'orders';
    
    // Primary key-kolumnen antas vara id
    protected $primaryKey = 'id';
    
    // Primary key-kolumnen antas vara auto-inkrementerande
    public $incrementing = false;

    // Vi vitlistar kolumner
    protected $fillable = [
        "increment_id",
        "created_at",
        "updated_at",
        "customer_id",
        "customer_email",
        "status",
        "marking",
        "grand_total",
        "subtotal",
        "tax_amount",
        "billing_address_id",
        "shipping_address_id",
        "shipping_method",
        "shipping_amount",
        "shipping_tax_amount",
        "shipping_description",
        "id",
        "invoice_id"
    ];

    public function shipping_address() {
        return $this->hasOne(Shipping_address::class);
    }

    public function billing_address() {
        return $this->hasOne(Billing_address::class);
    }

    public function item() {
        return $this->hasMany(Item::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function invoice() {
        return $this-belongsTo(Invoice::class);
    }
}