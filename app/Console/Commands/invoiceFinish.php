<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Order;

class invoiceFinish extends Command
{
	
	/**
	* The name and signature of the console command.
	     *
	     * @var string
	     */
	    protected $signature = 'invoice:finish {invoice_id}';
	
	
	/**
	* The console command description.
	     *
	     * @var string
	     */
	    protected $description = 'Command description';
	
	
	/**
	* Create a new command instance.
	     *
	     * @return void
	     */
	    public function __construct()
	    {
		parent::__construct();
	}
	
	
	/**
	* Execute the console command.
	     *
	     * @return mixed
	     */
	    public function handle()
	    {
		
		$invoice_id = $this->argument('invoice_id');
		$date = date('Y-m-d');
		$expiry_date = date('Y-m-d', strtotime($date . '+30 days'));
		
		$order = DB::table('orders')->where('invoice_id', $invoice_id)->get();
		
		$shipping_vat = 0;
		$shipping = 0;
		$total_excl_tax = 0;
		$vat = 0;

		$last_invoice = DB::table('invoices')->max('serial_number');
		$year = substr($last_invoice, 0, 2);
		$number = substr($last_invoice, 2, 4);
	   
		if ($last_invoice !== null && $year === date('y')) {
			$number++;
		} elseif ($last_invoice !== null && $year !== date('y')) {
			$year = date('y');
			$number = 1000;
		} else {
			$year = date('y');
			$number = 1000;
		}
		
		$serial_number = $year . $number;
				
		foreach ($order as $orders) {
			$customer_id = $orders->customer_id;
			$shipping_vat += $orders->shipping_tax_amount;
			$shipping += $orders->shipping_amount;
			$total_excl_tax += $orders->grand_total;
			$vat += $orders->tax_amount;
		}
		
		$grand_total = $shipping_vat + $shipping + $total_excl_tax + $vat;
		
		DB::table('invoices')
		            ->where('id', $invoice_id)
		            ->update([
		                "expiry_date" => $expiry_date,
		                "invoice_date" => $date,
		                "customer_id" => $customer_id,
		                "shipping_excl_vat" => $shipping,
		                "shipping_vat" => $shipping_vat,
		                "total_excl_vat" => $total_excl_tax,
		                "vat" => $vat,
		                "invoiced" => true,
		                "grand_total" => $grand_total,
		                "serial_number" => $serial_number
		            ]);
	}
}
