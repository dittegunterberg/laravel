<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Order;
use App\Invoice;
use DB;

class invoiceConnect extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'invoice:connect {invoice_id} {order_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $invoice_id = $this->argument('invoice_id');
        $order_id = $this->argument('order_id');

        $this->info("Sending request");
        $order['invoice_id'] = $invoice_id;

        DB::table('orders')
        ->where('id', $order_id)
        ->update(['invoice_id' => $invoice_id]);
    }
}