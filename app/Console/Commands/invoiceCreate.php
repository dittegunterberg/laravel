<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Invoice;

class invoiceCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'invoice:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Sending request");
        $invoice = Invoice::create();
        $this->info("Invoice created");
        $invoice->save();
    }
}
