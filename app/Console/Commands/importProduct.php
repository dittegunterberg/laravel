<?php

namespace App\Console\Commands;

use DB;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use App\Product;
use App\Group;
use App\Group_price;

class ImportProduct extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:products {file_name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import products';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $file = $this->argument('file_name');
        
        $storage = Storage::get($file);
        $response = json_decode($storage, true);

        foreach ($response['products'] as $products) {
            $this->info("Importing product with id: " . $products['entity_id']);
            $product = Product::findOrNew($products['entity_id']);
            $product->fill($products);
            $product->fill($products['stock_item']);
            $product->save();

            foreach ($products['group_prices'] as $group_prices) {
                $group_price = Group_price::findOrNew($group_prices['price_id']);
                $group_price['product_id'] = $product['entity_id'];
                $group_price->fill($group_prices);
                $group_price->save();
            }
        }

        foreach ($response['groups'] as $groups) {
            $group = Group::findOrNew($groups['customer_group_id']);
            $group->fill($groups);
            $group->save();
        }
    }
}