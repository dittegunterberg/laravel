<?php

namespace App\Console\Commands;

use DB;
use Illuminate\Console\Command;
use App\Shipping_address;
use App\Billing_address;
use App\Order;
use App\Item;

class ImportOrder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:order {url}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import orders';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $url = $this->argument('url');

        //CURL förfrågan, slänger headern och ger bara innehållet
        $this->info("Initializing curl...");
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $response = json_decode(curl_exec($curl), true);

        foreach ($response as $orders) {
            $this->info("Importing order with id: " . $orders['id']);

            if ($orders['status'] != 'processing') continue; {
            $order = Order::findOrNew($orders['id']);
            $order->fill($orders);
            $order->save();
            }

            if (isset($orders['shipping_address']) && is_array($orders['shipping_address'])) {
                $shipping_address = Shipping_address::findOrNew($orders['shipping_address']['id']);
                $shipping_address->fill($orders['shipping_address']);
                $shipping_address->save();
            }

            if (isset($orders['billing_address']) && is_array($orders['billing_address'])) {
                $billing_address = Billing_address::findOrNew($orders['billing_address']['id']);
                $billing_address->fill($orders['billing_address']);
                $billing_address->save();
            }

            foreach ($orders['items'] as $item) {
                $items = Item::findOrNew($item['id']);
                $items->fill($item);
                $items->save();
            }
        }
    }
}