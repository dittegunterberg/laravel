<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $table = 'invoices';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = false;

    protected $fillable = [
        "invoice_date",
        "expiry_date",
        "customer_id",
        "total_excl_vat",
        "vat",
        "shipping_excl_vat",
        "shipping_vat",
        "grand_total",
        "invoiced",
        "serial_number"
    ];

    public function customer() {
        return $this->hasOne(Customer::class);
    }

    public function order() {
        return $this->hasMany(Order::class);
    }
}